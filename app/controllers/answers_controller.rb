class AnswersController < ApplicationController
  before_filter :subscribe!, only: [:index, :show, :new, :edit, :create, :update, :destroy]
  before_action :admin_user!, only: [:edit, :update, :destroy]
  before_action :set_question, only: [:index, :show, :new, :edit, :create, :update, :destroy]
  before_action :set_answer, only: [:show, :edit, :update, :destroy]

  # GET /answers
  # GET /answers.json
  def index
    @answers = @question.answers
  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # GET /answers/new
  def new
    @question = Question.find(params[:question_id])
    @answer = @question.answers.new
  end

  # GET /answers/1/edit
  def edit
  end

  # POST /
  # POST /answers.json
  def create
    if Time.parse(session[:start_time]) + 80.second >= Time.now
      @answer = @question.answers.new(answer_params)

      if current_user
        @answer.user_id = current_user.id

        respond_to do |format|
          if @answer.save
            format.html { redirect_to question_answer_path(@question, @answer), notice: 'Answer was successfully created.' }
            format.json { render :show, status: :created, location: @answer }
            format.js{}
          else
            format.html { render :new }
            format.json { render json: @answer.errors, status: :unprocessable_entity }
            format.js{}
          end
        end
      else
        session[:last_question_text] = @answer.text
        session[:last_question] = @question.id
        respond_to do |format|
          format.js{}
        end
      end
    else
      respond_to do |format|
        format.js{}
      end
    end
  end

  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    respond_to do |format|
      if @answer.update(answer_params)
        format.html { redirect_to question_answer_path(@answer.question, @answer), notice: 'Answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @answer }
        format.js{ render 'answers/update', :locals => {:answer => @answer } }
      else
        format.html { render :edit }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to question_answers_path(@question), notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /new_time
  def new_time
    session[:start_time] = Time.now
    render plain: "OK"
  end

  # GET /check_time
  def check_time

    show = true
    if current_user
      if current_question.answers.where(:user_id => current_user.id).empty?
        if session[:start_time].nil?
          session[:start_time] = Time.now
        else
          if session[:start_time] > current_question.time
            show = false
          end
        end
      else
        show = false
      end
    else
      if session[:start_time].nil?
        session[:start_time] = Time.now
      else
        if session[:start_time] > current_question.time
          show = false
        end
      end
    end
    logger.info "#{session[:start_ime]}"
    logger.info "#{show.to_s} SHOW"

    respond_to do |format|
      format.js {render "answers/check_time", :locals => {:show => show}}
    end
  end

  def rating
    @users = User.all
    @questions = Question.where('time < ?', current_question.time ).order(:id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:question_id])
    end

    def set_answer
      @answer = @question.answers.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:text, :status, :question_id)
    end
end
