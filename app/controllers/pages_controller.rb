class PagesController < ApplicationController

  before_filter :empty_mail!

  def main
    if current_user 
      unless session[:last_question].nil? 
        @question = Question.find(session[:last_question])
        @answer = @question.answers.new(:text => session[:last_question_text], :user_id => current_user.id)
        @answer.save
        session[:last_question] = nil
        session[:last_question_text] = nil
      end
    end
    @answer = Answer.new
    @subscribe = Subscribe.new
  end

  def play
    if current_user 
      unless session[:last_question].nil? 
        @question = Question.find(session[:last_question])
        @answer = @question.answers.new(:text => session[:last_question_text], :user_id => current_user.id)
        @answer.save
        session[:last_question] = nil
        session[:last_question_text] = nil
      end
    end
    @answer = Answer.new
    @subscribe = Subscribe.new
    @report = Report.new
  end

  def vk
    @vk = VkontakteApi::Client.new(session['vk_token'])
  end

  def vkusers
    @vkgroups = Vkgroup.all
    @vkgroups.each do |g|
      Vkuser.create(vk_id: g.user_id)
    end
    render text: 'Done!'
  end

  def vkdata
    @vk = VkontakteApi::Client.new(session['vk_token'])
    @vkusers = Vkuser.all
    @vkusers.each do |u|
      sleep 0.34
      vk = @vk.execute(code: 'return API.users.get({user_ids:' + u.vk_id + ', fields: "sex,bdate"});').to_a
      u.name = vk[0].first_name
      u.surname = vk[0].last_name
      u.sex = vk[0].sex
      u.bdate = vk[0].bdate
      u.save
    end
  end
end
