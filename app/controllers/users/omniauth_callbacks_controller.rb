class Users::OmniauthCallbacksController < ApplicationController
  def facebook
    @user = User.find_for_facebook_oauth request.env["omniauth.auth"], cookies[:writename]
    cookies.delete :writename
    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in_and_redirect @user, :event => :authentication
    else
      flash[:notice] = "authentication error"
      redirect_to root_path
    end
  end

  def vkontakte
    session['vk_token'] = request.env["omniauth.auth"].credentials.token
    @user = User.find_for_vkontakte_oauth request.env["omniauth.auth"], cookies[:writename]
    cookies.delete :writename
    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Vkontakte"
      sign_in_and_redirect @user, :event => :authentication
    else
      flash[:notice] = "authentication error"
      redirect_to root_path
    end
  end

  def failure
    flash[:alert] = 'Вы отменили авторизацию через социальные сети'
    if current_user
      redirect_to user_path(current_user)
    else
      redirect_to root_path
    end
  end
end
