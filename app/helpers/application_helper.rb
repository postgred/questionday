module ApplicationHelper

  def title(title)
    if title.nil?
      title = 'Вопрос дня'
    end
    title
  end

  def subscribe!
    if request.original_fullpath !='/' && request.original_fullpath != '/users/sign_in'
      unless current_user
        redirect_to root_path
      end
    end
  end

  include UsersHelper
  include QuestionsHelper
end
