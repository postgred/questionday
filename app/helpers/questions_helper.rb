module QuestionsHelper

  def current_question
    Question.where('time <= ?', (Time.now).strftime('%Y-%m-%d %H:%M:%S')).last
  end
end
