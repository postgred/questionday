module UsersHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  def resource_class
    devise_mapping.to
  end

  def admin_user!
    unless current_user.is_admin?
      redirect_to root_path
    end
  end

  def empty_mail!
    if current_user && (current_user.email['vk.com'] == 'vk.com' || current_user.email['fb.com'] == 'fb.com')
      redirect_to email_user_path(current_user)
    end
  end
end
