class ApplicationMailer < ActionMailer::Base
  default from: "robot@mindpowergame.com"
  layout 'mailer'
end
