class DeliveriesMailer < ApplicationMailer

  def new_delivery(text, subject, sub_id)
    @text = text
    @subject = subject
    @sub_id = sub_id
    @to = 'users'
    User.all.each do |u|
      unless u.subs.where(:id => @sub_id).empty?
        if u.email['vk.com'] != 'vk.com' && u.email['fb.com'] != 'fb.com'
          @token = u.user_subscriptions.where(:subscription_id => sub_id).first.token
          mail to: u.email, subject: subject
        end
      end
    end
    @to = 'subscribers'
    Subscribe.all.each do |s|
      @token = s.token
      mail to: s.email, subject: @subject
    end
  end
end
