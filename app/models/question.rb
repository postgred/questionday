class Question < ActiveRecord::Base
  validates :title, presence: true
  validates :text, presence: true
  validates :answer, presence: true
  validates :user_id, presence: true
  validates :audio, presence: true
  validates :time, presence: true

  mount_uploader :audio, AudioUploader
  mount_uploader :image, ImageUploader

  has_many :answers, dependent: :destroy
  has_many :reports, dependent: :destroy
end
