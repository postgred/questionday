class Subscribe < ActiveRecord::Base
  validates :email, presence: true
end
