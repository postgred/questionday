class User < ActiveRecord::Base

  establish_connection 'mind'

  include QuestionsHelper

# Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  has_many :questions
  has_many :answers, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :user_subscriptions, foreign_key: "user_id"
  has_many :subs, through: :user_subscriptions, :source => :subscription

  def is_admin?
    self.is_admin
  end

  def rating
    self.answers.includes(:question)
        .where('questions.time < ?', current_question.time).references(:questions)
        .where(:status => true)
        .count
  end

  def self.find_for_facebook_oauth(access_token, writename)
    if user = User.where(:email => access_token.extra.raw_info.email).first || user = User.where(:fb => access_token.extra.raw_info.id).first
      if writename
        user.name = access_token.info.first_name
        user.surname = access_token.info.last_name
      end
      user.fb = access_token.extra.raw_info.id
      user.save
      user
    else
      logger.info "#{access_token}"
      if access_token.extra.raw_info.email.nil?
        User.create!(:name => access_token.info.first_name, :surname => access_token.info.last_name, :provider => access_token.provider, :username => access_token.extra.raw_info.name, :nickname => access_token.extra.raw_info.username, :email => rand.to_s + '@fb.com', :password => Devise.friendly_token[0,20])
      else
        User.create!(:name => access_token.info.first_name, :surname => access_token.info.last_name, :provider => access_token.provider, :username => access_token.extra.raw_info.name, :nickname => access_token.extra.raw_info.username, :email => access_token.extra.raw_info.email, :password => Devise.friendly_token[0,20])
      end
    end
  end

  def self.find_for_vkontakte_oauth(access_token, writename)
    if user = User.where(:email => access_token.info.email).first || user = User.where(:vk => access_token.extra.raw_info.id).first
      if writename
        user.name = access_token.info.first_name
        user.surname = access_token.info.last_name
      end
      user.vk = access_token.extra.raw_info.id
      user.save
      user
    else
      if access_token.info.email.nil?
        User.create!(:name => access_token.info.first_name, :surname => access_token.info.last_name, :provider => access_token.provider, :url => access_token.info.urls.Vkontakte, :username => access_token.info.name, :nickname => access_token.extra.raw_info.domain, :email => rand.to_s + '@vk.com', :password => Devise.friendly_token[0,20])
      else
        User.create!(:name => access_token.info.first_name, :surname => access_token.info.last_name, :provider => access_token.provider, :url => access_token.info.urls.Vkontakte, :username => access_token.info.name, :nickname => access_token.extra.raw_info.domain, :email => access_token.info.email, :password => Devise.friendly_token[0,20])
      end
    end
  end
end
