class UserSubscription < ActiveRecord::Base

  validates :token, presence: true

  belongs_to :user
  belongs_to :subscription
end
