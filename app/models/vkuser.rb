class Vkuser < ActiveRecord::Base
  validates :vk_id, uniqueness: true
end
