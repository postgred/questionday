json.array!(@deliveries) do |delivery|
  json.extract! delivery, :id, :topic, :text, :subscription_id
  json.url delivery_url(delivery, format: :json)
end
