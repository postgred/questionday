json.array!(@reports) do |report|
  json.extract! report, :id, :tech_comment, :tech_rate, :audio_comment, :audio_rate, :question_comment, :question_rate, :equp_comment, :sentence
  json.url report_url(report, format: :json)
end
