Rails.application.routes.draw do

  resources :deliveries
  resources :user_subscriptions do
    member do
      get 'unsubscribe'
    end
  end
  resources :subscriptions
  resources :vkgroups
  root "pages#main"
  resources :reports
  resources :subscribes do
    member do
      get 'unsubscribe'
    end
  end
  devise_for :users, controllers: { registrations: "registrations", :omniauth_callbacks => "users/omniauth_callbacks" }
  resources :users do
    member do
      get 'email'
    end
  end
  resources :questions do
    resources :answers
  end
  get '/rating', to: 'answers#rating'

  get '/new_time', to: 'answers#new_time'
  get '/check_time', to: 'answers#check_time'
  get '/offer', to: 'pages#offer'
  get '/faq', to: 'pages#faq'
  get '/endgame', to: 'pages#endgame'

  get '/play', to: 'pages#play'
  get '/vk', to: 'pages#vk'
  get '/vkusers', to: 'pages#vkusers'
  get '/vkdata', to: 'pages#vkdata'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
