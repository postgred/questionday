class AddAdmininfoToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :answer, :text
    add_column :questions, :comment, :string
    add_column :questions, :user_id, :integer
  end
end
