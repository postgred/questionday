class AddTimeToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :time, :timestamp
  end
end
