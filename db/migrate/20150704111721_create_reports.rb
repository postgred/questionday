class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.text :tech_comment
      t.integer :tech_rate
      t.text :audio_comment
      t.integer :audio_rate
      t.text :question_comment
      t.integer :question_rate
      t.text :equip_comment
      t.text :sentence

      t.timestamps null: false
    end
  end
end
