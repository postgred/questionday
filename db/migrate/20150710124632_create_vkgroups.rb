class CreateVkgroups < ActiveRecord::Migration
  def change
    create_table :vkgroups do |t|
      t.string :group_id
      t.string :user_id

      t.timestamps null: false
    end
  end
end
