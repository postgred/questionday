class CreateVkusers < ActiveRecord::Migration
  def change
    create_table :vkusers do |t|
      t.string :vk_id

      t.timestamps null: false
    end
  end
end
