class AddColumnsToVkusers < ActiveRecord::Migration
  def change
    add_column :vkusers, :name, :string
    add_column :vkusers, :surname, :string
    add_column :vkusers, :sex, :integer
    add_column :vkusers, :bdate, :string
  end
end
