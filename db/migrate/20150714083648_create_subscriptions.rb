class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :title
      t.string :sub_type

      t.timestamps null: false
    end
  end
end
