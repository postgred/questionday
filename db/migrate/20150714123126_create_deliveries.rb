class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.string :topic
      t.text :text
      t.integer :subscription_id

      t.timestamps null: false
    end
  end
end
