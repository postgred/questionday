class AddTokenToUsersubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :token, :string, uniq: true
  end
end
